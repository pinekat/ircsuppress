#!/usr/bin/make -f

CC=gcc
LIBS=purple glib-2.0

PKG_CONFIG=pkg-config

PKG_CFLAGS:=$(shell $(PKG_CONFIG) --cflags $(LIBS) || echo "FAILED")
ifeq ($(PKG_CFLAGS),FAILED)
	$(error "$(PKG_CONFIG) failed")
endif
CFLAGS+=$(PKG_CFLAGS) -fPIC -DPIC

PKG_LDLIBS:=$(shell $(PKG_CONFIG) --libs $(LIBS) || echo "FAILED")
ifeq ($(PKG_LDLIBS),FAILED)
	$(error "$(PKG_CONFIG) failed")
endif

PLUGIN_DIR_PURPLE	=  $(shell $(PKG_CONFIG) --variable=plugindir purple)
DATA_ROOT_DIR_PURPLE	=  $(shell $(PKG_CONFIG) --variable=datarootdir purple)

TARGET=libircsuppress.so

COMPILE.c = $(CC) $(CFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c

CFLAGS+=-Wall -g -O0 -Werror
CFLAGS += -DPURPLE_PLUGINS 
CFLAGS += -DGLIB_DISABLE_DEPRECATION_WARNINGS

all: $(TARGET)

clean:
	rm -f *.o $(OBJECTS) $(OBJECTS:.o=.d) $(TARGET)

install:
	mkdir -p $(DESTDIR)$(PLUGIN_DIR_PURPLE)
	install -m 664 $(TARGET) $(DESTDIR)$(PLUGIN_DIR_PURPLE)

$(TARGET): ircsuppress.o
	$(LINK.o) -shared $^ $(LOADLIBES) $(LDLIBS) -o $@

%.o: %.c
	$(COMPILE.c) $(OUTPUT_OPTION) $<
